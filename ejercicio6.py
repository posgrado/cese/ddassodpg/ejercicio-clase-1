def list_to_csv(lista):
    file = open("archivo.csv", "w+")
    file.write("name,address,age\n")
    for line in lista:
        file.write(str(line).strip("()").replace("'","") + "\n")
    file.close

lista = [('George', '4312 Abbey Road', 22), ('John', '54 Love Ave', 21)]
list_to_csv(lista)