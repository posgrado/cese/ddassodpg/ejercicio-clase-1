def file_to_dic():
    file = open('config.txt', 'r')
    dictionary = {}
    for line in file.readlines(): 
        if line.__contains__("="):
            values = line.strip("\n").split("=")
            dictionary[values[0]] = values[1]
    file.close
    return dictionary

print(file_to_dic())