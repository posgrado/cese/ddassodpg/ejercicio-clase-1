def dic_to_file(dictionary, file_name):
    file = open(file_name, 'w+')
    for item in dictionary: 
        file.write("{}={}\n".format(item, dictionary[item]))
    file.close
    
dictionary = {"color":"red", "state":True, "id":27}
dic_to_file(dictionary, "archivo.txt")