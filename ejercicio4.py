def es_par(numero):
    if (numero % 2) == 1:
        return False
    else:
        return True

print("Es par? {0}".format(es_par(2)))
print("Es par? {0}".format(es_par(1)))